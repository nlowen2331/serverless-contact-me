// Copyright 2019 Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

var aws = require("aws-sdk");
var ses = new aws.SES({ region: "us-east-2" });
var request = require('request')


exports.handler = async function (event) {
  
  console.log(event)
  
  //handle cors
  
  if(event.requestContext.http.method==='OPTIONS'){
    console.log(`Granted acceptable headers for CORS request`)
      return({
        statusCode: 200,
        headers:{
           'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Authorization,Content-Type',
            'Access-Control-Allow-Method': 'GET,POST,OPTIONS'
        }
      })
  }
  
  if(!process.env.VERIFICATION_TIMEOUT||!process.env.SECRET){
    console.log(`Missing envirenment variables`)
    return({
      statusCode: 500,
      body: JSON.stringify({message: 'Internal server error'})
    })
  }
  
  //Parse the body, reject if not application/json
  let body
  try{
    body=JSON.parse(event.body)
  }
  catch(err){
    console.log(`Unexpected request type`)
    return({
      statusCode: 400,
      body: JSON.stringify({message: 'Could not interpret body, please use application/json'})
    })
  }
  
  //Check that the body contains correct information, reject if missing fields
  if(!body.text||!body.subject||!body.name||!body.token){
    console.log(`Not acceptable: ${event.body}`)
    return({
        statusCode: 406,
        body: JSON.stringify({message: 'Missing fields'})
    })
  }
  
  //Verify the reCAPTCHA token

  //Create form

  let Form={
    secret: process.env.SECRET,
    response: body.token
  }

  let VerificationError=false
  let VerificationResponse
  try{
    VerificationResponse=await new Promise((resolve,reject)=>{
        request.post({url: 'https://www.google.com/recaptcha/api/siteverify', formData: Form},(err,response,body)=>{
            if(err){
              reject(err)
            }
            else{
              resolve(body)
            }
        })
    })
  }
  catch(err){
    VerificationError=err
  }
  
  if(VerificationError){
    console.log(`Token verification http error: ${VerificationError}`)
    return({
      statusCode: 500,
      body: JSON.stringify({message: 'Error communicating with other services'})
    })
  }
  
  VerificationResponse=JSON.parse(VerificationResponse)
  
  if(!VerificationResponse.success||VerificationResponse['error-codes']){
    console.log(`Google returned an error code`)
    VerificationResponse['error-codes'].forEach(err=>{
      console.log(err)
    })
    return({
      statusCode: 401,
      body: JSON.stringify({message: 'Could not verify your humanity'})
    })
  }
    
  //Set up request to SES
  const params = {
    Destination: {
      ToAddresses: [process.env.RECIEVING_EMAIL1,process.env.RECIEVING_EMAIL2],
    },
    Message: {
      
      Body: {
        Text: { Data: body.text },
      },
      
      Subject: { Data: `${body.name} - ${body.subject}`} },
      
    Source: process.env.SOURCE_EMAIL,
  };
  
  
  let SesResponse
  let SesError=null
  
  try{
      SesResponse = await ses.sendEmail(params).promise()
  }
  catch(err){
    SesError=err
  }
  
  if(SesError){
    console.log(`Error occured ${SesError}`)
      return ({
          statusCode: 500,
          body: JSON.stringify({message: 'Error communicating with other services'})
      })
  }
  
  console.log(process.env.EMAIL)
  console.log(`Success`)
  console.log(SesResponse)
  return ({
      statusCode: 200,
      body: JSON.stringify({message: 'OK'})
  })
  
};
